'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    rigger = require('gulp-rigger'),
    rimraf = require('rimraf'),
    browserSync = require('browser-sync'),
    postcss = require('gulp-postcss'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    imageminOptipng = require('imagemin-optipng'),
    cssnano = require('cssnano'),
    postshort = require('postcss-short'),
    precss = require('precss'),
    assets  = require('postcss-assets'),
    autoprefixer = require('autoprefixer'),
    inlinesvg = require('postcss-inline-svg'),
    sass = require('gulp-sass'),
    clearfix = require('postcss-clearfix'),
    reload = browserSync.reload;

var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/static/img/',
        pic: 'build/pic/',
        fonts: 'build/fonts/',
        fullpage: 'build/',
        favicon: 'build/favicon/',
        php: 'build/phpTemplates'
    },
    src: {
        html: 'src/*.html',
        js: 'src/js/*.js',
        style: 'src/style/*.*css',
        img: 'src/static/img/**/*.*',
        pic: 'src/pic/**/*.*',
        fonts: 'src/fonts/*.*',
        fullpage: 'src/full-page/**/*.*',
        favicon: 'src/favicon/*.*',
        php: 'src/phpTemplates/**/*.php'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        style: 'src/style/**/*.*css',
        img: 'src/static/img/**/*.*',
        pic: 'src/pic/**/*.*',
        fonts: 'src/fonts/*.*',
        fullpage: 'src/full-page/**/*.*',
        favicon: 'src/favicon/*.*',
        php: 'src/phpTemplates/**/*.php'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: "./build"
    },
    routes: {
        "/bower_components": "/"
    },
    tunnel: false,
    host: 'localhost',
    port: 9000,
    logPrefix: "Frontend_Devil"
};

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('css:build', function () {
    var processors = [
        postshort(),
        clearfix(),
        inlinesvg(),
        assets(),
        autoprefixer(),
        cssnano({core: false,
                zindex: false,
                reduceIdents: false})
    ];
    return gulp.src(path.src.style)
        .pipe(sass({
            includePaths: ['src/style/'],
            outputStyle: 'compressed',
            sourceMap: true,
            errLogToConsole: true
        }))
        .pipe(postcss(processors))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function () {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
        .pipe(reload({stream: true}));
});

gulp.task('favicon:build', function () {
    gulp.src(path.src.favicon)
        .pipe(gulp.dest(path.build.favicon))
        .pipe(reload({stream: true}));
});

gulp.task('php:build', function () {
    gulp.src(path.src.php)
        .pipe(gulp.dest(path.build.php))
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});

gulp.task('pic:build', function () {
    gulp.src(path.src.pic)
        .pipe(gulp.dest(path.build.pic))
        .pipe(reload({stream: true}));
});

gulp.task('fullpage:build', function () {
    gulp.src(path.src.fullpage)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.fullpage))
        .pipe(reload({stream: true}));
});

gulp.task('build', [
    'html:build',
    'js:build',
    'css:build',
    'image:build',
    'pic:build',
    'fonts:build',
    'fullpage:build',
    'favicon:build',
    'php:build'
]);


gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('css:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.pic], function(event, cb) {
        gulp.start('pic:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
    watch([path.watch.fullpage], function(event, cb) {
        gulp.start('fullpage:build');
    });
    watch([path.watch.favicon], function(event, cb) {
        gulp.start('favicon:build');
    });
    watch([path.watch.php], function(event, cb) {
        gulp.start('php:build');
    });

});


gulp.task('default', ['build', 'webserver', 'watch']);
