<div class="scheme-popup js-scheme-popup">
    <div class="store">
        <div class="store__inner">
            <div class="store__left">
                <a href="stores-inner.html" class="store__logo">
                    <img src="/pic/store/store-4.png" class="store__logo-img" alt="Магазин «Мир красок»" />
                </a>
                <div class="store__section">P-101</div>
            </div>
            <div class="store__right">
                <a class="store__link" href="stores-inner.html">
                    <div class="store__name">Мир Красок</div>
                    <div class="store__category">Краски</div>
                    <div class="store__contacts">+7 (495) 921-02-20 (доб.2321)</div>
                </a>
                <div class="store__tags">
                    <a href="/stores.html#section=section-2&category=category-1">#Краска</a>,
                    <a href="/stores.html#section=section-2&category=category-1">#Лак</a>,
                    <a href="/stores.html#section=section-2&category=category-1">#Эмаль</a>,
                    <a href="/stores.html#section=section-2&category=category-1">#Мастика</a>,
                    <a href="/stores.html#section=section-2&category=category-1">#Олифа</a>,
                    <a href="/stores.html#section=section-2&category=category-1">#Грунтовки</a>,
                    <a href="/stores.html#section=section-2&category=category-1">#Шпатлевки</a>
                </div>
            </div>
        </div>
    </div>
    <div class="intr-scheme__arrow"></div>
</div>
