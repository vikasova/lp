<div class="slide sl-event js-grid-item js-flt-event">
    <!-- Img size 580x400 px -->
    <img class="sl-event__preview" src="/pic/slider/slide-1.png" alt="">
    <div class="sl-event__inner">
        <div class="sl-event__info">
            <div class="sl-event__info-top">
                <div class="sl-event__date">12 сентября — 31 октября</div>
                <div class="sl-event__title">
                    <h3>МАСТЕР-КЛАССЫ ПО&nbsp;РЕМОНТУ</h3>
                </div>
                <div class="sl-event__descr">
                    Как правильно сделать укладку пола?<br /> Как подготовить<br /> стены к&nbsp;ремонту?
                </div>
            </div>
            <div class="sl-event__info-bottom">
                <a href="/advert-inner.html" class="btn btn--black">Подробнее</a>
            </div>
        </div>
    </div>
</div>

<div class="slide sl-news js-grid-item js-flt-news">
    <div class="sl-news__inner">
        <div class="sl-news__preview">
            <img class="sl-news__preview-img" src="/pic/slider/slide-news.png" alt="">
        </div>
        <div class="sl-news__info">
            <div class="sl-news__info-top">
                <div class="sl-news__date">1 февраля</div>
                <div class="sl-news__title">
                    <h3>ТОРЖЕСТВЕННОЕ
                    ОТКРЫТИЕ НОВОГО
                    ПАВИЛЬОНА</h3>
                </div>
                <div class="sl-news__descr">
                    Призы, конкурсы, подарки! Приходи с&nbsp;друзьями и&nbsp;получи скидку!
                </div>
            </div>
            <div class="sl-news__info-bottom">
                <div class="slide-status">Мероприятие закончено</div>
            </div>
        </div>
    </div>
</div>

<div class="slide sl-advert-full js-grid-item js-flt-advert">
    <div class="sl-advert-full__inner">
        <div class="sl-advert-full__info">
            <div class="sl-advert-full__logo">
                <img class="sl-advert-full__logo-img"
                     src="/pic/slider/slide-advert-logo.png"
                    alt="Brand">
            </div>
            <div class="sl-advert-full__info-top">
                <div class="sl-advert-full__title">
                    скидки до
                    <span class="sl-advert-full__title-big">50%</span>
                </div>
                <div class="sl-advert-full__descr">
                    Мы&nbsp;ценим своих клиентов и&nbsp;часто радуем
                    их&nbsp;замечательными акциями.<br/>
                    Если вам нравится экономить и&nbsp;при этом покупать
                    изысканную, высококачественную мебель&nbsp;&mdash; у&nbsp;нас есть,
                    что вам предложить!
                </div>
            </div>
            <div class="sl-advert-full__info-bottom">
                <div class="slide-status">Акция закончена</div>
            </div>
        </div>
    </div>
</div>

<div class="slide sl-advert js-grid-item js-flt-advert">
    <div class="sl-advert__inner">
        <div class="sl-advert__preview">
            <img class="sl-advert__preview-img" src="/pic/slider/slide-advert.png" alt="">
        </div>
        <div class="sl-advert__info">
            <div class="sl-advert__info-top">
                <div class="sl-advert__icon"></div>
                <div class="sl-advert__descr">
                    Лучшие скидки недели, от&nbsp;лучших магазинов в&nbsp;городе. И&nbsp;ещё какой то&nbsp;текст, который более подробно расскажет про акции.
                </div>
            </div>
            <div class="sl-advert__info-bottom">
                <a href="/advert-inner.html" class="btn btn--white">Посмотреть</a>
            </div>
        </div>
    </div>
</div>
