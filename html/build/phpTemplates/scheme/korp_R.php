<div class="scheme-group">

    <!-- Блок с переключателями этаже -->
    <div class="scheme-group__head">
        <div class="scheme-group-btn scheme-group-btn--big js-sch-group-btn" data-item='all'>
            <span class="scheme-group-btn__letter">P</span>
            <span class="scheme-group-btn__text">корпус</span>
        </div>
        <div class="scheme-group-btn js-sch-group-btn" data-item='1'>
            <span class="scheme-group-btn__text">1 этаж</span>
        </div>
        <div class="scheme-group-btn js-sch-group-btn" data-item='2'>
            <span class="scheme-group-btn__text">2 этаж</span>
        </div>
    </div>
    <!--  END Блок с переключателями этаже -->

    <!-- Блок с картами.
         Каждая отдельная карта (этаж) должен быть в блоке .scheme-group__item.js-group-item с указанием data-item="n".
         data-item нужен для связки карты и кнопки этажа. У кнопки показа всего корпуса data-item="all".
     -->
    <div class="scheme-group__body">
        <div class="scheme-group__item js-group-item" data-item='1'>
            <img src="/pic/scheme/korp_R-1.png" width="100%" border="0" usemap="#korp_R1">
            <map name="korp_R1">
                <area class="sector" rel="R101" href="#" shape="poly" coords="260,241,260,308,347,409,424,408,425,242" />
                <area class="sector" rel="R102" href="#" shape="poly" coords="429,240,515,240,516,408,427,408" />
                <area class="sector" rel="R103" href="#" shape="poly" coords="517,240,605,240,606,408,518,411" />
                <area class="sector" rel="R104" href="#" shape="poly" coords="607,241,723,242,723,339,692,340,689,408,608,408" />
                <area class="sector" rel="R105" href="#" shape="poly" coords="841,240,842,409,754,408,755,338,725,339,725,240" />
                <area class="sector" rel="R106" href="#" shape="poly" coords="932,241,931,409,844,410,844,241" />
                <area class="sector" rel="R107" href="#" shape="poly" coords="1020,279,1020,408,934,409,935,279" />
                <area class="sector" rel="R108" href="#" shape="poly" coords="934,241,1102,240,1101,409,1022,407,1019,282,935,278" />
            </map>
        </div>
        <div class="scheme-group__item js-group-item" data-item='2'>
            <img src="/pic/scheme/korp_R-2.png" usemap="#korp_R2" />
            <map name="korp_R2">
                <area class="sector" rel="R201" href="#" shape="poly" coords="258,255,423,254,422,423,336,423,256,325" />
                <area class="sector" rel="R202" href="#" shape="poly" coords="426,255,513,255,514,386,425,385" />
                <area class="sector" rel="R203" href="#" shape="poly" coords="516,255,603,254,603,386,515,383" />
                <area class="sector" rel="R204" href="#" shape="poly" coords="605,254,720,255,720,340,689,339,687,387,605,385" />
                <area class="sector" rel="R205" href="#" shape="poly" coords="723,255,839,255,839,386,751,385,752,342,722,341" />
                <area class="sector" rel="R206" href="#" shape="poly" coords="841,255,1100,254,1098,424,841,423" />
            </map>
        </div>
    </div>
</div>
