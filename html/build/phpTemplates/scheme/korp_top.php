<div class="scheme-group">
    <div class="count-store-block" id="count-store-block">
        <div class="count-store js-count-store" id="mag_R" style="left: 665px; top: 65px;">
            4 магазина
        </div>
        <div class="count-store js-count-store" id="mag_E" style="left: 965px; top: 45px;">
            5 магазинов
        </div>
        <div class="count-store js-count-store" id="mag_M" style="left: 980px; top: 135px;">
            30
        </div>
        <div class="count-store js-count-store" id="mag_O" style="left: 825px;
        top: 290px;">
            10 магазинов
        </div>
        <div class="count-store js-count-store" id="mag_N" style="left: 325px;
        top: 455px;">
            15 магазинов
        </div>
        <div class="count-store js-count-store" id="mag_T" style="left: 420px; top: 90px;">
            1 магазин
        </div>
        <div class="count-store js-count-store" id="mag_D" style="left: 200px;
        top: 130px;">
            7 магазинов
        </div>
    </div>
    <div class="scheme-group__head"></div>
    <div class="scheme-group__body">
        <div class="scheme-group__item js-group-item is-active" data-item='1'>
            <img src="/pic/scheme/top.jpg" usemap="#index" id="img_index" width="100%">
            <map name="index" id="top-scheme">
                <area target="Корпус Н" rel="N" href="#" shape="poly" coords="331,313,18,314,43,275,37,275,68,230,73,230,75,229,102,187,123,181,263,177,281,184,278,226,258,233,95,237,74,271,305,273,332,284" />
                <area target="Корпус Д" rel="D" href="#" shape="poly" coords="99,161,146,159,188,80,155,79" />
                <area target="Корпус Т" rel="T" href="#" shape="poly" coords="198,145,219,90,218,84,210,84,220,62,340,61,343,69,361,69,361,77,370,76,378,110,371,114,375,131,335,136,335,142" />
                <area target="Корпус Р" rel="R" href="#" shape="poly" coords="370,62,379,50,412,53,403,62,538,75,531,83,383,69" />
                <area target="Корпус Е" rel="E" href="#" shape="poly" coords="559,70,577,48,680,56,671,81" />
                <area target="Корпус О" rel="O" href="#" shape="poly" coords="397,263,383,204,420,204,422,199,454,196,457,202,489,204,539,135,643,148,606,261,602,269,591,273,579,275,494,278,490,273,484,274,486,277,446,276,448,273,438,273,438,276,420,274,406,270" />
                <area target="Корпус М" rel="M" href="#" shape="poly" coords="682,96,578,86,543,133,667,154" />
            </map>
        </div>
    </div>
</div>
