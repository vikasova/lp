<img src="/pic/scheme/korp_E.jpg" border="0" usemap="#korp_E">
<map name="korp_E">
<area class="sector" shape="poly" coords="88,150,132,150,182,201,182,211,179,211,179,242,163,221,88,221" rel="E127" target="E127" nohref="nohref">
<area class="sector" shape="poly" coords="178,104,317,104,317,122,357,122,357,156,271,156,246,173,209,173,159,122" rel="E128" target="E128" nohref="nohref">
<area class="sector" shape="poly" coords="248,172,273,155,357,155,357,207,248,207" rel="E129" target="E129" nohref="nohref">
<area class="sector" shape="rect" coords="248,209,357,242" rel="E130" target="E130" nohref="nohref">
<area class="sector" shape="poly" coords="248,242,357,242,357,294,318,294,318,312,263,312,263,278,248,278" rel="E131" target="E131" nohref="nohref">
<area class="sector" shape="rect" coords="213,312,264,277" rel="E131А" target="E131А" nohref="nohref">
<area class="sector" shape="rect" coords="179,278,214,312" rel="E132" target="E132" nohref="nohref">
<area class="sector" shape="poly" coords="154,245,154,245,177,278,177,312,154,312" rel="E132А" target="E132А" nohref="nohref">
<area class="sector" shape="rect" coords="102,245,122,312" rel="E135" target="E135" nohref="nohref">
<area class="sector" shape="rect" coords="122,245,154,312" rel="E133" target="E133" nohref="nohref">
<area class="sector" shape="poly" coords="81,245,101,245,101,312,53,312,53,288,81,288" rel="E136" target="E136" nohref="nohref">
<area class="sector" shape="rect" coords="53,245,80,286" rel="E136А" target="E136А" nohref="nohref">
<area class="sector" shape="poly" coords="82,672,108,672,108,692,82,697" rel="E016" target="E016" nohref="nohref">
<area class="sector" shape="poly" coords="108,672,134,672,134,689,108,692" rel="E017" target="E017" nohref="nohref">
<area class="sector" shape="rect" coords="136,646,169,689" rel="E018" target="E018" nohref="nohref">
<area class="sector" shape="rect" coords="170,647,191,689" rel="E019" target="E019" nohref="nohref">
<area class="sector" shape="poly" coords="165,625,219,625,219,676,217,676,217,663,192,663,192,645,165,645" rel="E020" target="E020" nohref="nohref">
<area class="sector" shape="rect" coords="253,581,336,613" rel="E024" target="E024" nohref="nohref">
<area class="sector" shape="rect" coords="253,614,336,646" rel="E025" target="E025" nohref="nohref">
<area class="sector" shape="rect" coords="253,647,336,679" rel="E026" target="E026" nohref="nohref">
<area class="sector" shape="rect" coords="253,680,336,711" rel="E027" target="E027" nohref="nohref">
<area class="sector" shape="poly" coords="220,711,336,711,336,726,317,726,317,743,220,743" rel="E028" target="E028" nohref="nohref">
<area class="sector" shape="rect" coords="173,711,219,743" rel="E029" target="E029" nohref="nohref">
<area class="sector" shape="rect" coords="135,711,173,743" rel="E030" target="E030" nohref="nohref">
<area class="sector" shape="poly" coords="70,720,135,711,135,734,70,734" rel="E031" target="E031" nohref="nohref">
<area shape="rect" coords="230,767,247,793" rel="out1" target="out1" nohref="nohref">
<area shape="rect" coords="229,506,247,536" rel="out2" target="out2" nohref="nohref">
<area shape="rect" coords="83,118,110,134" rel="out3" target="out3" nohref="nohref">
<area shape="rect" coords="52,174,88,220" rel="cafe" target="cafe" nohref="nohref">
<area shape="poly" coords="136,582,155,582,155,548,220,548,220,613,136,613" rel="bank1" target="bank1" nohref="nohref">
<area shape="poly" coords="253,548,318,548,318,564,335,564,335,580,253,580" rel="bank2" target="bank2" nohref="nohref">
<area shape="poly" coords="136,647,165,647,165,625,220,625,220,614,136,614" rel="bank3" target="bank3" nohref="nohref">
<area shape="poly" coords="71,629,134,629,135,670,82,670,82,642,71,642" rel="WC" target="WC" nohref="nohref">

</map>
<style>
.e127{left: 138px; top: 200px;}
.e128{left: 221px; top: 151px;}
.e129{left: 260px; top: 180px;}
.e130{left: 260px; top: 218px;}
.e131{left: 260px; top: 254px;}
.e131a{left: 222px; top: 282px;}
.e132{left: 186px; top: 282px;}
.e132a{font-size: 10px; left: 152px; top: 282px;}
.e133{left: 126px; top: 250px;}
.e135{left: 102px; top:261px;}
.e136{left: 82px; top: 250px;}
.e136a{left: 53px; top: 261px;}

.e016{left: 86px; top: 672px;}
.e017{left: 113px; top: 673px;}
.e018{left: 143px; top: 667px;}
.e019{left: 171px; top: 668px;}
.e020{left: 193px; top: 627px;}
.e024{left: 264px; top: 591px;}
.e025{left: 265px; top: 622px;}
.e026{left: 265px; top: 653px;}
.e027{left: 265px; top: 685px;}
.e028{left: 265px; top: 718px;}
.e029{left: 186px; top: 716px;}
.e030{left: 144px; top: 717px;}
.e031{left: 98px; top: 717px;}
</style>
<div class="sector_name e127" rel="E127">127</div>
<div class="sector_name e128" rel="E128">128</div>
<div class="sector_name e129" rel="E129">129</div>
<div class="sector_name e130" rel="E130">130</div>
<div class="sector_name e131" rel="E131">131</div>
<div class="sector_name e131a" rel="E131А">131А</div>

<div class="sector_name e132" rel="E132">132</div>
<div class="sector_name e132a" rel="E132А">132А</div>
<div class="sector_name e133" rel="E133">133</div>
<div class="sector_name e135" rel="E135">135</div>
<div class="sector_name e136" rel="E136">136</div>
<div class="sector_name e136a" rel="E136А">136А</div>

<div class="sector_name e016" rel="E016">016</div>
<div class="sector_name e017" rel="E017">017</div>
<div class="sector_name e018" rel="E018">018</div>
<div class="sector_name e019" rel="E019">019</div>
<div class="sector_name e020" rel="E020">020</div>
<div class="sector_name e024" rel="E024">024</div>
<div class="sector_name e025" rel="E025">025</div>
<div class="sector_name e026" rel="E026">026</div>
<div class="sector_name e027" rel="E027">027</div>
<div class="sector_name e028" rel="E028">028</div>
<div class="sector_name e029" rel="E029">029</div>
<div class="sector_name e030" rel="E030">030</div>
<div class="sector_name e031" rel="E031">031</div>
