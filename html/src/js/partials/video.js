var videoInit = function() {

    if ( $('.js-video').length === 0 ) return;

    var videoContainer = $('.js-video');

    videoContainer.each(function() {
        var video = $(this).children().get();
        console.log(video);
        plyr.setup(video, {
           controls: ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume', 'fullscreen']
       });
    });
    
}();
