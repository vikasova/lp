var droplistInit = function() {
	'use strict';

	if (!$('.js-droplist').length) return;

	var droplist = $('.js-droplist'),
		droplistHead = droplist.find('.js-droplist-head');

	// Открытие droplist
	droplistHead.on('click', function() {

		var parent = $(this).closest('.js-droplist');

		if (parent.hasClass('is-disabled')) return;

		if (!parent.hasClass('is-opened')) {
			droplist.filter('.is-opened').removeClass('is-opened');
			parent.addClass('is-opened');
		} else {
			parent.removeClass('is-opened');
		}
	});

	// Выбор значения droplist. Если есть класс js-droplist-checkbox,
	// то можно выбрать несколько значений
	var initClickItem = function(item) {
		item.on('click', function() {
			var self = $(this),
				parent = $(this).closest('.js-droplist'),
				items = parent.find('.js-droplist-item'),
				value = $(this).data('value'),
				text = $(this).text(),
				arrayValue = '';

			if (parent.hasClass('js-droplist-checkbox')) {
				self.toggleClass('is-selected');

				if (value === '*') {
					if (self.hasClass('is-selected')) {
						items.each(function() {
							if ($(this).data('value') === '*') return;
							arrayValue += (!arrayValue ? '' : ', ') + $(this).data('value');
							$(this).addClass('is-selected');
						})
						parent.find('.js-droplist-input').val(arrayValue).trigger('change');
						parent.find('.js-droplist-head').text('Все');
					} else {
						arrayValue = '';
						items.removeClass('is-selected');
						parent.find('.js-droplist-input').val('').trigger('change');
						parent.find('.js-droplist-head').text('Бренды');
					}

				} else {
					items.filter('[data-value="*"]').removeClass('is-selected');
					var checked = items.filter('.is-selected');
					if (checked.length) {
						checked.each(function() {
							arrayValue += (!arrayValue ? '' : ', ') + $(this).data('value');
						})
						text = arrayValue.replace(/[,]/g, " /");
						parent.find('.js-droplist-input').val(arrayValue).trigger('change');
						parent.find('.js-droplist-head').text(text);
					} else {
						parent.find('.js-droplist-head').text('Бренды');
					}
				}

			} else if (!self.hasClass('is-selected')) {
				items.filter('.is-selected').removeClass('is-selected');
				parent.find('.js-droplist-input').val(value).trigger('change');
				parent.find('.js-droplist-head').text(text);
				parent.removeClass('is-opened');
				self.addClass('is-selected');
			}

		});
	};

	// Закрытие droplist
	var closeDroplist = function() {
		droplist.filter('.is-opened').removeClass('is-opened');
	};

	$('.js-droplist-close').on('click', closeDroplist);

	// Блокировка droplist, если значения отсутвуют
	var checkDroplistItemsLenght = function() {

		droplist.each(function() {
			var items = $(this).find('.droplist__item');
			if (!items.length) {
				$(this).addClass('is-disabled');
			} else {
				$(this).removeClass('is-disabled');
			}
		});

	};

	// Обновления параметров (позиционирование, ширина) droplist, которые занимают всю ширину контейнера (с классом droplist--full)
	var updateParamsDroplist = function() {
		var fullDroplist = $('.js-droplist.droplist--full'),
			wrapper = $('.wrapper');

		fullDroplist.each(function() {

			var droplistBody = $(this).find('.js-droplist-body'),
				droplistHead = $(this).find('.js-droplist-head'),
				droplistArrow = $(this).find('.js-droplist-arrow'),
				offsetLeft = $(this).offset().left - wrapper.offset().left - (wrapper.outerWidth() - wrapper.width()) / 2,
				arrowOffsetLeft = droplistHead.offset().left + droplistHead.outerWidth() - 59 - wrapper.offset().left - (wrapper.outerWidth() - wrapper.width()) / 2;

			droplistBody.css({
				"width": wrapper.width(),
				"left": offsetLeft * (-1)
			});
			droplistArrow.css({
				left: arrowOffsetLeft
			})

		});
	};

	// Проверка наличия droplist--full
	if ($('div.js-droplist.droplist--full')) {
		updateParamsDroplist();
		$(window).on('resize', function() {
			updateParamsDroplist();
		});
	}

	return {
		initMainAction: function(items) {
			initClickItem(items);
			checkDroplistItemsLenght();
		}
	}

}();

if ($('.js-droplist').length) {
	droplistInit.initMainAction($('.js-droplist-item'));
}
