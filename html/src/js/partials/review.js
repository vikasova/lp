var review = function() {

	if (!$('#review-form').length) return;

	var form = $('#review-form'),
		submitBtn = $('#review-submit');

	// Валидация полей. Привязка к полю идет по классу

	// input name
	$.validator.addClassRules('js-review-name', {
		required: true,
		minlength: 2
	});

	// input contacts
	$.validator.addClassRules('js-review-contacts', {
		required: true,
		eitherEmailPhone: true
	});

	// input comment
	$.validator.addClassRules('js-review-comment', {
		required: true,
		minlength: 2
	});

	// Сообщения для каждого типа проверки
	$.validator.messages.required = 'Это поле обязательное';
	$.validator.messages.minlength = 'Минимальная длина поля - 2 символа';
	$.validator.messages.eitherEmailPhone = 'Email или телефон указан неверно';

	// Метод для проверки контактный данных
	$.validator.addMethod("eitherEmailPhone", function(value, element) {
		isPhone = this.optional(element) || /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(value);
		isEmail = this.optional(element) || /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(value);

		return isPhone || isEmail;

	}, "Email или телефон указан неверно");

	// Отправка отзыва
	submitBtn.on('click', function(e) {
		e.preventDefault();

		// Проверка валидации формы
		if (!$(form).valid()) {
			return;
		}

		$(this).addClass('btn--loaded');

		// Этот код удалить. Он для тестирования
		messageAction('Отзыв успешно отправлен!');
		//


		// Этот код раскомментировать.

		// $.ajax({
		// 	'type': 'post',
		// 	'url': form.attr('action'),
		// 	'data': form.serialize(),
		// 	'dataType': 'json',
		// 	success: function(response) {
		// 		if (response.status === 'success') {
		// 			messageAction('Отзыв успешно отправлен!');
		// 		} else {
		// 			messageAction('Произошла ошибка. Попробуйте оставить отзыв позже.');
		// 		}
		// 	}
		// });
	});

	// Сообщение об отправки отзыва
	var messageAction = function(message) {
		var messageWrap = $('#form-message'),
			messageText = messageWrap.find('.js-form-message-text');

		messageText.text(message);

		messageWrap.fadeIn(function() {
			// Сброс значений формы и droplist
			form[0].reset();
			if ($('.js-droplist').length) {
				form.find('.js-droplist-item.is-selected').removeClass('is-selected');
				form.find('.js-droplist-head').text('Категория отзыва');
				form.find('.js-droplist-input').val('');
			}
			submitBtn.removeClass('btn--loaded');
		});

		setTimeout(function() {
			messageWrap.fadeOut();
			messageText.text('');
		}, 4000);

	};

}();
