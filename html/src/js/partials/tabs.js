// Табы
function tabs(elem) {

    function init(tabTrigger, tabItem) {
        tabTrigger.filter('[data-tab="0"]').addClass('is-active');
        tabTrigger.filter('[data-subtab="0"]').addClass('is-active');
        tabItem.eq(0).addClass('is-active').fadeIn();
    }

    elem.each(function() {
        var $this = $(this),
            triggers = $this.find('.js-tabs-trigger'),
            items = $this.find('.js-tabs-item'),
            indexTab = 0,
            indexSubtab = 0,
            hasSubtab = $this.data('subtab');

        function changeTab() {
            if ($(this).hasClass('is-active')) return;

            var $this = $(this);

            // Снятие выделения у tab
            if ($this.data('tab') !== undefined) {
                triggers.filter('[data-tab="' + indexTab + '"]').removeClass('is-active');
                indexTab = $this.data('tab');
            }

            // Снятие выделения у subtab
            if ($this.data('subtab') !== undefined && hasSubtab) {
                triggers.filter('[data-subtab="' + indexSubtab + '"]').removeClass('is-active');
                indexSubtab = $this.data('subtab');
            }

            $this.addClass('is-active');
            items.filter('.is-active').removeClass('is-active').fadeOut(function() {
                if (hasSubtab) {
                    items.filter('[data-tab="' + indexTab + '"][data-subtab="' + indexSubtab + '"]').addClass('is-active').fadeIn();
                } else {
                    items.filter('[data-tab="' + indexTab + '"]').addClass('is-active').fadeIn();
                }
            });
        }

        triggers.on('click', changeTab);

        init(triggers, items);
    });

}

tabs($('.js-tabs'));
