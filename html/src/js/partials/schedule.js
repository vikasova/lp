// Выделение ближайшего времени в расписании
function schedule(elem) {

    elem.each(function() {
        var $this = $(this),
            items = $this.find('.js-schedule-item'),
            time = new Date(),
            timeSec = time.getHours() * 3600 + time.getMinutes() * 60,
            min = 0;

        items.each(function() {
            var itemTime = $(this).text().split(/:/),
                itemTimeSec = parseInt(itemTime[0]) * 3600 + parseInt(itemTime[1]) * 60;

            if (itemTimeSec > timeSec) {
                $(this).addClass('is-active');
                return false;
            }
        });
    });
}

schedule($('.js-schedule'));
