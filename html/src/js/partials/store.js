var store = function() {

	if (!$('#store-filters').length) return;

	var section = $('#store-filters'), // основной блок
		//url = $('#store-form').data('action'),
		sectionBtn = $('.js-store-section'), // кнопки секций
		droplistCategory = $('.js-droplist-category'), // выпадающий список с категориями
		droplistSubcategory = $('.js-droplist-subcategory'), // выпадающий список с подкатегориями
		droplistBrands = $('.js-droplist-brand'), // выпадающий список с брендами
		searchBtn = $('.js-store-search'), // кнопка поиска по брендам
		storeWrapper = $('.js-store-list'), // обертка для списка магазинов
		article = $('.js-article-wrapper'), // обертка для сео данных
		href =  window.location.href, // начальный адрес страницы
		filters = {  // объект для сбора параметров фильтрации
			section: '',
			category: '',
			subcategory: '',
			brands: '',
		},
		statusChanges = { // объект для проверки обновления фильтров.
			category: '', // если установлено "set", то выпадающие списки не обновляются
			subcategory: '',
			brands: ''
		};

	// Прелоадер. Блокирует блок с фильтрами во время их обновления
	var loadingAjax = function(status) {
		if (status) {
			section.addClass('is-disable');
		} else {
			section.removeClass('is-disable');
		}
	}

	sectionBtn.on('click', function() {
		if ($(this).hasClass('is-active')) return;

		var section = $(this).data('section');

		// Если выбрано "Показать все", то все сбрасываем
		// и выводим все магазины
		if (section === 'all') {
			filters = {
				section: 'all',
				category: '',
				subcategory: '',
				brands: ''
			};
		} else {
			filters = {
				section: section,
				category: '',
				subcategory: '',
				brands: ''
			};
		}

		sectionBtn.filter('.is-active').removeClass('is-active');
		$(this).addClass('is-active');
		statusChanges.category = '';
		statusChanges.subcategory = '';
		statusChanges.brands = '';
		loadInfo(section, filters);
	});

	droplistCategory.on('change', 'input', function() {
		var value = $(this).val();
		filters.category = value;
		statusChanges.category = 'set';
		statusChanges.subcategory = '';
		statusChanges.brands = '';
		loadInfo($(this).val(), filters);
	});

	droplistSubcategory.on('change', 'input', function() {
		var value = $(this).val();
		filters.subcategory = value;
		statusChanges.subcategory = 'set';
		statusChanges.brands = '';
		loadInfo($(this).val(), filters);
	});

	droplistBrands.on('change', 'input', function() {
		var value = $(this).val().replace(/\s+/g,'');
		filters.brands = value;
		searchBtn.removeClass('is-disable');
		statusChanges.subcategory = 'set';
	});

	searchBtn.on('click', function() {
		loadInfo(url, filters);
	})

	// Сброс данных выпадающего списка.
	// Если параметро item отсутствует, то сбрасываются все.
	// Нужно на случай, если загружаются данные по новому разделу,
	// а какие-то фильтры уже применены и их нужно сбросить.
	var resetDroplist = function(item) {
		if (item) {
			item.find('.js-droplist-head').text(item.data('title'));
			item.find('.droplist__body-wrapper').html('');
			item.find('input').val('');
		} else {
			$('.js-droplist').each(function() {
				$(this).find('.js-droplist-head').text($(this).data('title'));
				$(this).find('.droplist__body-wrapper').html('');
				$(this).find('input').val('');
			})
		}
	};

	// Применение фильтрации.
	// Формируется url в зависимости от выбранных фильтров.
	var loadInfo = function(url, filters) {
		console.log(url);
		loadingAjax(true);
		// Формирование url
		if (filters.brands) {
			url += "?brands=" + filters.brands;
		}


		$.ajax({
			type: 'get',
			url: url,
			dataType: "json",
			success: function(response) {
				// Ответ:
				// response.category - список категорий
				// response.subcategory - список подкатегорий
				// response.brands - список брендов
				// response.stores - список магазинов
				// respnse.seo - сео информация

				// Если ответ содержит нужне данные - подставляем их.
				// Если установлено значение в statusChanges, то этот
				// элемент не обновляется. Например, если мы сменили подкатегорию, то нам не нужно обновлять категорию и саму подкатегорию, а только обновляем список брендов, магазинов и сео.
				console.log(response);
				if (response.category && !statusChanges.category) {
					resetDroplist();
					droplistCategory.find('.droplist__body-wrapper').html(response.category);
				}

				if (response.subcategory && !statusChanges.subcategory) {
					if (filters.category) {
						resetDroplist(droplistSubcategory);
						resetDroplist(droplistBrands);
						droplistSubcategory.find('.droplist__body-wrapper').html(response.subcategory);
					}
				}

				if (response.brands && !statusChanges.brands) {
					resetDroplist(droplistBrands);
					droplistBrands.find('.droplist__body-wrapper').html(response.brands);
					searchBtn.addClass('is-disable');
				} else {
					searchBtn.addClass('is-disable');
				}

				if (response.stores) {
					storeWrapper.html(response.stores);
				} else{
					storeWrapper.html("")
				}

				if (response.seo) {
					article.html(response.seo);

				} else {
					article.html('');
				}
				var title = $('.js-title').text(), newLink = url;
				console.log(newLink);
				window.history.replaceState({}, title, newLink);

				droplistInit.initMainAction($('.js-droplist-item'));
				loadingAjax(false);
			}

		});
	};

	// Загрузка данных при переходе с другой страницы.
	// Параметры фильтрации записаны в hash
	var loadingOnHash = function() {
		var hash = window.location.hash.substr(1),
			str = { // объект для хранения "масок" фильтрации
				section: 'section=',
				category: 'category=',
				subcategory: 'subcategory=',
				brands: 'brands='
			},
			sectionHash, categoryHash, subcategoryHash, brandsHash; // Переменные для хранения фильтров, полученных из hash

		if (!hash) return;

		// Проверяем, есть ли вхождение маски из str в hash
		// Если есть, то проверяем наличие связки фильтров с помощью & (т.е. есть набор фильтров или он всего один)
		// Если вхождение есть, записываем его в глобальны объект filters.
		if (hash.indexOf(str.section) >= 0) {
			if (hash.indexOf("&") >= 0) {
				sectionHash = hash.substring( str.section.length, hash.indexOf("&") );
				hash = hash.substring( hash.indexOf("&") + 1, hash.length );
			} else {
				sectionHash = hash.substring( str.section.length, hash.length );
			}
			filters.section = sectionHash;
		}

		if (hash.indexOf(str.category) >= 0) {
			if (hash.indexOf("&") >= 0) {
				categoryHash = hash.substring( str.category.length, hash.indexOf("&") );
				hash = hash.substring( hash.indexOf("&") + 1, hash.length );
			} else {
				categoryHash = hash.substring( str.category.length, hash.length );
			}
			filters.category = categoryHash;
		}

		if (hash.indexOf(str.subcategory) >= 0) {
			if (hash.indexOf("&") >= 0) {
				subcategoryHash = hash.substring( str.subcategory.length, hash.indexOf("&") );
				hash = hash.substring( hash.indexOf("&") + 1, hash.length );
			} else {
				subcategoryHash = hash.substring( str.subcategory.length, hash.length );
			}
			filters.subcategory = subcategoryHash;
		}

		if (hash.indexOf(str.brands) >= 0) {
			if (hash.indexOf("&") >= 0) {
				brandsHash = hash.substring( str.brands.length, hash.indexOf("&") );
				hash = hash.substring( hash.indexOf("&") + 1, hash.length );
			} else {
				brandsHash = hash.substring( str.brands.length, hash.length );
			}
			filters.brands = brandsHash;
		}

		console.log(filters);

		// Удаляем hash из адресной строки
		window.location.hash = ''; // для старых браузеров
		history.pushState('', document.title, window.location.pathname);
		href = window.location.href;

		loadInfo(filters);
	}();

	// Обновление страницы, если мы выбрали фильтр в карточке магазина.
	$('.store__tags a').on('click', function() {
		location.reload();
	});

}();
