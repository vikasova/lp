var getContent = function() {
	var params = [{
			'name': 'advert',
			'url': '/phpTemplates/a-list.php'
		},
		{
			'name': 'price',
			'url': 'priceurl'
		},
		{
			'name': 'advice',
			'url': 'adviceurl'
		},
		{
			'name': 'interior',
			'url': 'interiorurl'
		},
	];

	var getUrl = function(name) {
		for (var i = 0; i < params.length; i++) {
			if (params[i].name === name) return params[i].url;
		}
	};

	$('.js-btn-load').on('click', function() {
		if ($(this).hasClass('btn--loaded')) return;

		var btn = $(this),
			dataName = $(this).data('typeContent'),
			url = getUrl(dataName);

		btn.addClass('btn--loaded');

		$.ajax({
			type: 'get',
			url: url,
			dataType: 'html',
			success: function(response) {
				// parseHtml используется для предпоказа
				// Если изначально передается объект, то можно заменить
				// parse на response
				var parse = $.parseHTML(response);

				$grid.append( parse ).isotope( 'appended', parse );
				btn.removeClass('btn--loaded');

			}
		});
	});

}();
