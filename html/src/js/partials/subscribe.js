var subscribe = function() {

	if (!$('#subscribe-form').length) return;

	var form = $('#subscribe-form'),
		submitBtn = $('#subscribe-submit'),
        formWrapper = $('.js-subscribe-wrapper'),
        showBtn = $('.js-subscribe-show');

	// Валидация полей. Привязка к полю идет по классу

	// input mail
	$.validator.addClassRules('js-subscribe-mail', {
		required: true,
		isEmail: true
	});

	// Сообщения для каждого типа проверки
	$.validator.messages.required = 'Это поле обязательное';
	$.validator.messages.isEmail = 'Email или телефон указан неверно';

	// Метод для проверки контактный данных
	$.validator.addMethod("isEmail", function(value, element) {
		isEmail = this.optional(element) || /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(value);

		return isEmail;

	}, "Email указан неверно");

	// Отправка отзыва
	submitBtn.on('click', function(e) {
		e.preventDefault();

		// Проверка валидации формы
		if (!$(form).valid()) {
			return;
		}

		$(this).addClass('btn--loaded');

		// Этот код удалить. Он для тестирования
		messageAction('Спасибо за подписку!');
		//


		// Этот код раскомментировать.

		// $.ajax({
		// 	'type': 'post',
		// 	'url': form.attr('action'),
		// 	'data': form.serialize(),
		// 	'dataType': 'json',
		// 	success: function(response) {
		// 		if (response.status === 'success') {
		// 			messageAction('Спасибо за подписку!');
		// 		} else {
		// 			messageAction('Произошла ошибка. Попробуйте подписаться позже.');
		// 		}
		// 	}
		// });
	});

	// Сообщение об отправки отзыва
	var messageAction = function(message) {
		var messageWrap = $('#form-message'),
			messageText = messageWrap.find('.js-form-message-text');

		messageText.text(message);

		messageWrap.fadeIn(function() {
			// Сброс значений формы и droplist
			form[0].reset();
			if ($('.js-droplist').length) {
				form.find('.js-droplist-item.is-selected').removeClass('is-selected');
				form.find('.js-droplist-head').text('Категория отзыва');
				form.find('.js-droplist-input').val('');
			}
			submitBtn.removeClass('btn--loaded');
		});

		setTimeout(function() {
			formWrapper.fadeOut(function() {
				messageWrap.fadeOut();
				messageText.text('');
				showBtn.removeClass('is-opened').text('Подписаться');
			});
		}, 4000);

	};

    showBtn.on('click', function() {

		var btn = $(this);

		if (!btn.hasClass('is-opened')) {
			btn.addClass('is-opened').text('Закрыть');
			formWrapper.fadeIn();
		} else {
			btn.removeClass('is-opened').text('Подписаться');
			formWrapper.fadeOut(function() {
				form[0].reset();
			});
		}
    });

}();
