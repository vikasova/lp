var galleryInit = function() {

	if ($('#image-gallery').length === 0) return;

	var $gallery = $('#image-gallery'),
		gallerySlider = $gallery.children('.gallery-slider').lightSlider({
			gallery: true,
			item: 1,
			thumbItem: 6,
			slideMargin: 20,
			thumbMargin: 20,
			speed: 500,
			auto: false,
			adaptiveHeight: true,
			enableDrag: false,
			onSliderLoad: function() {
				//Задержка отображения галерии, на случай, если в ней есть видео
				//и требуется обновить высоту галереи
				if ($gallery.find('.js-video .plyr').length > 0) {
					setTimeout(function() {
						gallerySlider.refresh();
						$gallery.addClass('is-visible');
					}, 700);
				} else {
					$gallery.addClass('is-visible');
				}

			}
		});

	var slideTo = function(index) {
		gallerySlider.goToSlide(index);
	};

	return {
		slideTo: slideTo
	}

}();
