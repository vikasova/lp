var fixedHeader = function() {

    var headerWrapper = $('.js-header-wrapper'),
        headerSpace = $('.js-header-space'),
        nav = $('.js-nav'),
        header = $('.js-header'),
        navOffset = nav.offset().top,
        menuBtn = $('.js-menu-btn');

    var headerReaction = function() {
        if ($('body').width() <= 640) return;

        if (navOffset <= $(window).scrollTop() && !headerWrapper.hasClass('header--fixed')) {
            headerSpace.height(headerWrapper.height());
            headerWrapper.addClass('header--fixed');
        } else if (navOffset > $(window).scrollTop() && headerWrapper.hasClass('header--fixed')) {
            headerSpace.height(0);
            headerWrapper.removeClass('header--fixed');
        }

    };

    menuBtn.on('click', function() {
        if (!$(this).hasClass('is-opened')) {
            $(this).addClass('is-opened');
            headerWrapper.addClass('is-opened');
        } else {
            $(this).removeClass('is-opened');
            headerWrapper.removeClass('is-opened');
        }
    });

    headerReaction();
    $(window).on('DOMMouseScroll mousewheel', function(event) {
        headerReaction();

        if (event.originalEvent.detail < 0 || event.originalEvent.wheelDelta > 0) {
            if (!headerWrapper.hasClass('is-opened')) {
                headerWrapper.addClass('is-opened');
                menuBtn.addClass('is-opened');
            }

        }
    });

    $(window).on('load resize', function() {
        if ($('body').width() <= 640) {
            headerWrapper.addClass('header--fixed');
            headerSpace.height(nav.height());
        } else {
            navOffset = nav.offset().top;
            if (headerWrapper.hasClass('header--fixed')) {
                headerSpace.height(nav.outerHeight() + header.outerHeight());
                navOffset = header.outerHeight();
            }
        }
    });

}();
