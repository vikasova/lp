var mallScheme = function() {
	if (!$('#scheme').length) return;

	var scheme = $('#scheme'), // главный контейнер
		stores = $('#stores'), // контейнер для магазинов
		corpBtn = $('.js-corp-btn'), // кнопки переключения корпусов
		categoryBtn = $('.js-category'), // кнопки выбора категории
		// bodySize = [],
		// coordPseElement = [],
		currentCategory = 1, // текущая выбранная категория
		currentCorp = 'top'; // текущий корпус

	// Параметры подсветки областей
	var schemeParams = {
		fillColor: '402d8d',
		fillOpacity: 0.5,
		stroke: true,
		strokeColor: '402d8d',
		strokeWidth: 2,
		singleSelect: false
	};

	// Лоадер
	var loader = function(hide) {
		if (hide) {
			scheme.addClass('is-loading');
		} else {
			scheme.removeClass('is-loading');
		}
	}

	// Получаем номер текущего корпуса
	var getCurrentCorp = function() {
		return $('.js-corp-btn.is-active').data('corp');
	}

	var initSchemeGroupActions = function() {
		var schemeGroupBtns = scheme.find('.js-sch-group-btn'),
			schemeGroupItems = scheme.find('.js-group-item'),
			schemeHeight = 0,
			img = scheme.find('img');

		schemeGroupBtns.filter('[data-item="all"]').addClass('is-active');
		schemeGroupItems.fadeIn().promise().done(function() {
			$(this).addClass('is-active');
		});

		img.mapster(schemeParams);

		updateImageMap();

		schemeGroupBtns.each(function() {

			var btn = $(this);

			btn.on('click', function() {
				if ($(this).hasClass('is-active')) return;

				var dataItem = $(this).data('item');

				clearSelect();
				schemeGroupBtns.filter('.is-active').removeClass('is-active');
				$(this).addClass('is-active');

				if (dataItem === 'all') {
					schemeGroupItems.fadeIn().promise().done(function() {
						schemeGroupItems.addClass('is-active');
					});
				} else {
					schemeGroupItems.filter('.is-active').fadeOut().promise().done(function() {
						$(this).removeClass('is-active');
						schemeGroupItems.filter('[data-item="' + dataItem + '"]').fadeIn().promise().done(function() {
							$(this).addClass('is-active');
						});
					});
				}
			})
		});
	};

	// Обновление размеров схемы при resize
	var updateImageMap = function() {
		var image = scheme.find('img[usemap]'),
			newWidth = scheme.width(),
			newHeight = newWidth / image.width() * image.height();

		image.mapster('resize', newWidth, newHeight);
	};

	//Переключение корпусов
	corpBtn.on('click', function() {
		if ($(this).hasClass('is-active')) return;

		clearSelect();
		var rel = $(this).data('corp');
		corpBtn.filter('.is-active').removeClass('is-active');
		$(this).addClass('is-active');
		loadKorpus(rel);
	});

	// Загрузка корпуса
	var loadKorpus = function(rel, callback) {
		var korpus = rel;

		loader(true);

		switch (korpus) {
			case "R":
				file_korp = '/phpTemplates/scheme/korp_R.php';
				break;

			case "E":
				file_korp = '/phpTemplates/scheme/korp_E.php';
				break;

			case "M":
				file_korp = '/phpTemplates/scheme/korp_M.php';
				break;

			case "O":
				file_korp = '/phpTemplates/scheme/korp_O.php';
				break;

			case "N":
				file_korp = '/phpTemplates/scheme/korp_N.php?2';
				break;

			case "T":
				file_korp = '/phpTemplates/scheme/korp_T.php';
				break;

			case "D":
				file_korp = '/phpTemplates/scheme/korp_D.php';
				break;

			case "top":
				file_korp = '/phpTemplates/scheme/korp_top.php';
				break;
		}

		$.ajax({
			url: file_korp,
			success: function(data) {
				// Подгрузка схемы, например data.scheme
				scheme.html(data);

				// Подгрузка списка магазинов
				stores.html(data.stores);

				initStoreClick(stores.find('.js-store-scheme'));
				// bodySize = [$('body').width(), $('body').height()];
				initSchemeGroupActions();

				if (korpus === 'top') {
					coordCountStorePopup();
				}

				if (callback) {
					callback();
				}

				loader(false);
			}
		});
	};

	var initStoreClick = function(elem) {
		elem.on('click', function(e) {
			e.preventDefault();
			var sector = $(this).find('.store__section').text().replace('-', ''),
				area = scheme.find('area[rel="' + sector + '"]');

			if (!$(this).hasClass('is-active')) {
				$(this).addClass('is-active');
				showStoreInfo(area);
			} else {
				$(this).removeClass('is-active');
				clearSelect();
			}

		});
	};

	// Выбор категории
	categoryBtn.on('click', function() {
		if ($(this).hasClass('is-active')) return;

		var category = $(this).data('cat');

		loader(true);
		currentCategory = category;
		currentCorp = getCurrentCorp();

		categoryBtn.filter('.is-active').removeClass('is-active');
		$(this).addClass('is-active');

		if (currentCorp === 'top') {
			// Удалить эти строки
			$('#count-store-block').fadeIn();
			loader(false);
			// Удалить эти строки


			// Раскоментировать. Заменить url на нужный

			// area.each(function() {
			// 	$(this).mapster('select');
			// });
			// $.ajax({
			// 	type:"GET",
			// 	url:"get_mag_count.php",
			// 	dataType:"json",
			// 	data:"ID=" + category,
			// 	success: function(data, result)
			// 	{
			// 		if(result === 'success')
			// 		{
			// 			$.each(data, function(korpus, count){
			// 				$('#mag_' + korpus).html(count);
			// 			});
			//			$('#count-store-block').fadeIn();

			// 			loader(false);
			// 		}
			// 	}
			// });

		} else {

			// Раскоментировать. Заменить url на нужный

			// $.ajax({
			// 	type:"GET",
			// 	url:"get_mag_corp.php",
			// 	dataType:"json",
			// 	data:"ID=" + category +"&corp="+currentCorp,
			// 	success: function(data, result)
			// 	{
			// 		if(result=='success')
			// 		{
			// 			scheme.find('area').mapster('deselect');
			//
			// 			$.each(data, function(data, sector){
			// 				setTimeout(function() {
			// 					scheme.find('area[rel=' + sector + ']').mapster('select');
			// 				}, 100);
			// 			});
			// 		}

			// 		loader(false);
			// 	}
			// });
		}

	});

	// Вызов события по клику на главную схему
	$('body').on('click', '#top-scheme area', function(e) {
		e.preventDefault();
		var rel = $(this).attr('rel');

		loadKorpus(rel);
		corpBtn.filter('.is-active').removeClass('is-active');
		corpBtn.filter('[data-corp="' + rel + '"]').addClass('is-active');
	});

	// Вызов события по клику на схемы корпусов
	$('body').on('click', 'map .sector', function(e) {
		e.preventDefault();

		var sector = $(this);

		// Если сектор уже выбран, то снимаем выделение и сбрасываем
		// информацию о магазине.

		if (sector.mapster('get')) {
			showStoreInfo(sector);
		} else {
			// sector.mapster('deselect');
			clearSelect();
		}

	});

	// Очистка подсветки и инф-ии о магазине при клике на свободную область
	$('body').on('click', function(e) {
		var parent = e.target.closest('map') || e.target.closest('.js-scheme-popup');
		if (!parent) {
			clearSelect();
		};
	});

	// Очистка подсветки и инф-ии о магазине
	var clearSelect = function() {
		scheme.find('.js-scheme-popup').remove();
		scheme.find('.pse-element').remove();
		scheme.find('area').mapster('deselect');
	}

	// Загрузка инфо-ии о магазине
	var showStoreInfo = function(sector) {

		clearSelect();
		sector.mapster('select');

		var NaborCoordinat = sector.attr('coords'),
			nabor = NaborCoordinat.split(','),
			maxY = 0,
			minY = 0,
			maxX = 0,
			minX = 0,
			widthMap = sector.closest('.js-group-item').width(),
			heightMap = sector.closest('.js-group-item').height(),
			left, top,
			sectorWrapper = sector.closest('.js-group-item');

		// Найдем максимальные и миниальные значения координат выделенной области,
		// чтобы выводить popup по центру

		$.each(nabor, function(i, j) {
			if (i == 0) {
				minX = maxX = j;
			}
			if (i == 1) {
				minY = j;
				maxY = j;
			}
			i++;

			if (i % 2 == 0) {
				if (j < minY) minY = j;
				if (j > maxY) maxY = j;
			} else {
				if (j < minX) minX = j;
				if (j > maxX) maxX = j;
			}
		});

		minX = parseInt(minX);
		maxX = parseInt(maxX);
		minY = parseInt(minY);
		maxY = parseInt(maxY);

		// Добавляется скрытый элемент по центру выделенной области,
		// для позиционированя popup

		var coord = [minX + (maxX - minX) / 2, minY + (maxY - minY) / 2],
			pseElement =
			$('<div/>', {
				class: 'pse-element',
			}).appendTo(sectorWrapper);

		left = (coord[0] / widthMap * 100).toFixed(2);
		top = (coord[1] / heightMap * 100).toFixed(2);

		pseElement.css({
			left: left + '%',
			top: top + '%'
		});

		// Раскоментировать data, подставить нужный url
		$.ajax({
			type: "GET",
			url: "/phpTemplates/sector/store-101.php",
			// data:"sect=" + encodeURIComponent(sector.attr('rel')),
			success: function(data, result) {
				if (result === 'success') {

					sectorWrapper.append(data);
					initPopup(scheme.find('.js-scheme-popup'), pseElement);
				}

			}
		});
	};

	// Инициализация popup
	var initPopup = function(popup, reference) {
		var poper = new Popper(
			reference,
			popup, {
				placement: 'auto',
				modifiers: {
					preventOverflow: {
						boundariesElement: scheme[0]
					},
				}
			}
		);
	};

	// Обновление размеров схемы при resize window
	$(window).on('load resize', function() {
		updateImageMap();
	});

	// Подсчет left и top для popups главной схемы
	var coordCountStorePopup = function() {
		var popup = $('.js-count-store'),
			widthWrapper = 1180, // ширина схемы
			heightWrapper = 675; // высота схемы

		popup.each(function() {
			var left = (parseInt($(this).css("left")) / widthWrapper * 100).toFixed(2),
				top = (parseInt($(this).css("top")) / heightWrapper * 100).toFixed(2);

			$(this).css({
				left: left + '%',
				top: top + '%'
			})
		});
	};

	// Загрузка корпуса по ссылке с другой страницы
	var loadOnHash = function() {
		var hash = window.location.hash,
			corp, area, length;

		// Загружаем нужный корпус, если перешли по ссылки с hash
		if (hash) {
			hash = window.location.hash.split('#')[1];
			corp = hash[0];
			length = hash.length;
			area = hash.substr(1, length);

			loadKorpus(corp, function() {
				setTimeout(function() {
					var targetArea = scheme.find('area[rel="' + hash + '"]');
					targetArea.mapster('select');
					showStoreInfo(targetArea);
					corpBtn.filter('.is-active').removeClass('is-active').end().filter('[data-corp="' + corp + '"]').addClass('is-active');
				}, 800);
			});

			$('body, html').animate({
				scrollTop: scheme.offset().top
			}, 500);

		} else {
			// Если hash отсутсвует, то загружаем главную схему
			loadKorpus('top');
		}
	};

	loadOnHash();

}();
