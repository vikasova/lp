//Фильтры на страницах Акции, Советы, Интерьеры
var $grid = $('.js-grid').isotope({
	itemSelector: '.js-grid-item',
	layoutMode: 'fitRows',
	transitionDuration: '0.5s'
});

// Фильтрация контента
var initFilter = function() {
	var filter = $('div.js-filter');

	filter.on('click', function() {
		if ($(this).hasClass('filter-active')) return;

		var filterValue = $(this).data('filter');

		$(filter).filter('.filter-active').removeClass('filter-active');
		$(this).addClass('filter-active');

		$grid.isotope({
			filter: filterValue
		});

	});

}();

//Фильтры брендов
var brandFilterInit = function() {
	if ($('#brands').length === 0) return;

	var $brandBlock = $('#brands'),
		sortBtn = $('.js-brands-sort'),
		ctgBtn = $('.js-brands-ctg'),
		items = $('.js-brand'),
		sortType = $('.js-brands-sort.is-active').data('type'),
		$brandGrid;

	// Фильтрация брендов
	ctgBtn.on('click', function() {
		if ($(this).hasClass('is-active')) return;

		var filterValue = $(this).data('filter'),
			filterText = $(this).text(),
			sortType = $('.js-brands-sort.is-active').data('type'),
			blocks = $('.js-brands-block'),
			compliteFilter;

		blocks.removeClass('is-filtering');
		blocks.filter('[data-filter="' + filterValue + '"]').addClass('is-filtering');
		compliteFilter = '.is-filtering';

		if ($('.is-filtering').length === 0) {
			compliteFilter = '.not';
		}

		if (filterValue === "*") {
			compliteFilter = "*";
		}

		$(ctgBtn).filter('.is-active').removeClass('is-active');
		$(this).addClass('is-active');

		$brandGrid.isotope({
			filter: compliteFilter
		});

	});

	// Смена типа сортировка
	sortBtn.on('click', function() {
		if ($(this).hasClass('is-active')) return;

		$(sortBtn).filter('.is-active').removeClass('is-active');
		$(this).addClass('is-active');

		var sort = $(this).data('type');
		$('.brands__ctg-block').filter('.is-active').fadeOut().promise().done(function() {
			$(this).removeClass('is-active');
			$('.brands__ctg-block').filter('[data-type="' + sort + '"]').fadeIn().addClass('is-active');

			// Сбрасываем все фильтры, делаем активным первый фильтр
			$('.brands__ctg-block.is-active .js-brands-ctg').removeClass('is-active').eq(0).addClass('is-active');

			createFilterBlocks(sort);
		});

		$brandGrid.isotope({
			filter: '*'
		});
	});

	// Группируем бренды по блокам в зависимости от выбранной сортировки
	var createFilterBlocks = function(sort) {
		var oldContainer = $('.js-brands-container'),
			brandsContainer = $('<div />', {
				'class': 'brands__inner js-brands-container'
			});

		oldContainer.addClass('js-rmv-container');
		oldContainer.after(brandsContainer);

		ctgBtn.each(function() {
			var self = $(this),
				title = self.text(),
				filter = self.data('filter'),
				filterItems;

			if (filter === '*') return;

			if (sort === 'country') {
				filterItems = items.filter('[data-country="' + filter+ '"]');
			} else {
				filterItems = items.filter('[data-filter="' + filter+ '"]');
			}

			if (filterItems.length > 0) {
				var block = $('<div />', {
					'class': 'brands-block js-brands-block',
					'data-filter': filter
				}),
					brandsWrapper = $('<div />', {
					'class': 'brands__wrapper',
				}),
					brandsTitle = $('<div />', {
					'class': 'brands__filter-value',
					'text': title
				});
				brandsContainer.append(block);
				block.append(brandsTitle, brandsWrapper);
				brandsWrapper.append(filterItems);
				oldContainer.remove();
			}

		});

		$brandGrid = brandsContainer.isotope({
			itemSelector: '.js-brands-block',
			layoutMode: 'fitRows',
			transitionDuration: '0.5s'
		});


	};

	createFilterBlocks($('.js-brands-sort.is-active').data('type'));

}();
