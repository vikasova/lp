var initMap = function() {
	var myMap,
		myPlacemark;

	myMap = new ymaps.Map("map", {
		center: [55.677974, 37.766449],
		zoom: 15,
		controls: ['zoomControl']
	});

	myPlacemark = new ymaps.Placemark([55.678682, 37.781580], {
		iconCaption: 'Торговый центр "Люблинское поле"',
	}, {
		iconColor: 'red',
		preset: 'islands#redDotIconWithCaption'
	});

	myMap.geoObjects.add(myPlacemark);
	myMap.behaviors.disable('scrollZoom');
};

var initFullMap = function() {
	var myMap,
		myPlacemark,
		multiRoute,
		searchControl,
		pointB = [55.678682, 37.781580],
		pointA,
		routeType,
		routeBtn = $('.js-route-btn'),
		fullMapContainer = document.getElementById('full-map-container'),
		closeBtn = $('.js-close-map');

	myMap = new ymaps.Map("full-map", {
		center: [55.678682, 37.781580],
		zoom: 15,
		controls: ['zoomControl']
	});

	myPlacemark = new ymaps.Placemark([55.678682, 37.781580], {
		iconCaption: 'Торговый центр "Люблинское поле"',
	}, {
		iconColor: 'red',
		preset: 'islands#redDotIconWithCaption'
	});

	searchControl = new ymaps.control.SearchControl({
		options: {
			float: 'left',
			floatIndex: 100,
			noPlacemark: true
		}
	});

	myMap.geoObjects.add(myPlacemark);
	myMap.behaviors.disable('scrollZoom');
	myMap.controls.add(searchControl);

	// Когда пользователь выбирает адрес, срабатывает построение маршрута
	searchControl.events.add('resultselect', function(e) {
		pointA = searchControl.getRequestString();
		result(pointA, pointB);
	}, this);

	// Вывод маршрута
	var result = function(pointA, pointB) {
		multiRoute = new ymaps.multiRouter.MultiRoute({
			// Описание опорных точек мультимаршрута.
			referencePoints: [
				pointA,
				pointB
			],
			// Параметры маршрутизации.
			params: {
				// Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
				results: 2,
				routingMode: routeType
			}
		}, {
			// Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
			boundsAutoApply: true
		});

        myMap.geoObjects.add(multiRoute);
	};

	//Выбор типа маршрута
	routeBtn.on('click', function() {
		routeType = $(this).data('route');
		$(fullMapContainer).addClass('is-opened');
	});

	// Закрытие и отчистка карты
	closeBtn.on('click', function() {
		routeType = '';
		$(fullMapContainer).removeClass('is-opened');

		setTimeout(function() {
			myMap.geoObjects.removeAll();
			myMap.geoObjects.add(myPlacemark);
			myMap.setCenter(pointB);
			myMap.setZoom(15);
		}, 500);
	});
};

if ($('#map').length) {
	ymaps.ready(initMap);
	ymaps.ready(initFullMap);
}
