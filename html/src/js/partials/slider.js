var mainSlider = new Swiper('.js-slider', {
    pagination: '.swiper-pagination',
    slideClass: 'slide',
    prevButton: '.slider-btn-prev',
    nextButton: '.slider-btn-next',
    slidesPerView: 'auto',
    paginationClickable: true
});

var brandSlider = new Swiper('.js-brand-slider', {
    pagination: '.swiper-pagination',
    slideClass: 'brand-slide',
    prevButton: '.slider-btn-prev',
    nextButton: '.slider-btn-next',
    slidesPerView: 'auto',
    slidesPerGroup: '6',
    paginationClickable: true,
    breakpoints: {
    800: {
        slidesPerGroup: '4'
    },
    480: {
        slidesPerGroup: '2'
    }
  }
});
