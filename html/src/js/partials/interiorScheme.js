var interiorSchemeInit = function() {

	if ($('.intr-scheme').length === 0) return;

	var scheme = $('.js-intr-scheme'),
		allStores = $('div.js-store'),
		slider = $('#image-gallery');

	scheme.each(function() {
		var self = $(this),
			allReference = self.find('.js-scheme-btn'),
			allPopper = self.find('.js-scheme-popup'),
			clearBtn = self.find('.js-scheme-clear');

		allReference.each(function() {
			var index = $(this).data('popup');

			if (allPopper.filter('[data-popup=' + index + ']').length === 0) return;

			var reference = allReference.filter('[data-popup=' + index + ']'),
				popper = allPopper.filter('[data-popup=' + index + ']'),
				anotherPopper = new Popper(
					reference,
					popper, {
						placement: 'auto',
						modifiers: {
							preventOverflow: {
								boundariesElement: self[0]
							},
						}
					}
				);
		});

		var showPopup = function(target) {
			var index = target.data('popup'),
				lengthActive;

			if (!target.hasClass('is-active')) {
				allReference.filter('[data-popup=' + index + ']').addClass('is-active');
				allStores.filter('[data-popup=' + index + ']').addClass('is-active');
				allPopper.filter('[data-popup=' + index + ']').addClass('is-show');
			} else {
				allReference.filter('[data-popup=' + index + ']').removeClass('is-active');
				allStores.filter('[data-popup=' + index + ']').removeClass('is-active');
				allPopper.filter('[data-popup=' + index + ']').removeClass('is-show');
			}

			lengthActive = allPopper.filter('.is-show').length;

		};

		allReference.on('click', function() {
			showPopup($(this));
		});

		allStores.on('click', function() {
			showPopup($(this));
		});

		clearBtn.on('click', function() {
			if (!self.hasClass('show-points')) {
				self.addClass('show-points');
				$(this).text('Скрыть подсказки');
			} else {
				self.removeClass('show-points');
				$(this).text('Показать подсказки');
				hideAll();
			}
		});

		var hideAll = function() {
			allReference.removeClass('is-active');
			allStores.removeClass('is-active');
			allPopper.removeClass('is-show');

			for (var i = 0; i < allReference.length; i++) {
				var index = allReference.eq(i).data('popup');
				allStores.filter('[data-popup=' + index + ']').removeClass('is-active');
			}
		};

	});

	allStores.on('click', function() {
		var index = $(this).data('popup'),
			popupBtn = $('.js-scheme-btn').filter('[data-popup=' + index + ']'),
			popup = $('.js-scheme-popup').filter('[data-popup=' + index + ']'),
			slide = popupBtn.closest('li.lslide'),
			scheme = slide.find('.js-intr-scheme'),
			slideIndex = $('li.lslide').index(slide);

		console.log(scheme);

		if (!$(this).hasClass('is-active')) {
			if (!scheme.hasClass('show-points')) {
				scheme.addClass('show-points');
				scheme.find('.js-scheme-clear').text('Скрыть подсказки');
			}

			$('body, html').animate({
				scrollTop: scheme.offset().top - 100
			}, 500);

			galleryInit.slideTo(slideIndex);
			$(this).addClass('is-active');
			popupBtn.addClass('is-active');
			popup.addClass('is-show');
		} else {
			$(this).removeClass('is-active');
			popupBtn.removeClass('is-active');
			popup.removeClass('is-show');
		}


	});

}();
